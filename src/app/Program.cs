﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Amazon;
using Amazon.EC2;
using Amazon.EC2.Model;
using Amazon.Runtime;

namespace AwsSnapShot
{
  public static class Helper
  {
    public static void Dump(this object c)
    {
      Console.Out.WriteLine(c);
    }
  }


  internal class Program
  {
    private const string TagRetention = "AwSnapRetention";

    public static void Main(string[] args)
    {
      
      AmazonEC2Client amazonEc2Client = null;
      if (args.Length == 3)
      {
        $"Connect to AWS using basic auth [{args[0]}-{args[2]}].".Dump();
        amazonEc2Client = new AmazonEC2Client(new BasicAWSCredentials(args[0], args[1]), RegionEndpoint.EnumerableAllRegions.FirstOrDefault(x => x.SystemName == args[2]));
      }
      else {
        
         var access_key_id = Environment.GetEnvironmentVariable("AWS_ACCESS_KEY_ID");
         if (string.IsNullOrEmpty(access_key_id)) throw new Exception("Environment variable 'AWS_ACCESS_KEY_ID' required.");
         var secret_access_key = Environment.GetEnvironmentVariable("AWS_SECRET_ACCESS_KEY");
         if (string.IsNullOrEmpty(secret_access_key)) throw new Exception("Environment variable 'AWS_SECRET_ACCESS_KEY' required.");
         var default_region = Environment.GetEnvironmentVariable("AWS_DEFAULT_REGION");
         if (string.IsNullOrEmpty(default_region)) throw new Exception("Environment variable 'AWS_DEFAULT_REGION' required.");
        
        $"Connect to AWS using environment variables [{access_key_id}-{default_region}].".Dump();

        amazonEc2Client = new AmazonEC2Client(RegionEndpoint.EnumerableAllRegions.FirstOrDefault(x => x.SystemName == default_region));
       
      }
      MainAsync(amazonEc2Client).Wait();
    }

    #region Private Methods

    private static async Task MainAsync(AmazonEC2Client ec2Client)
    {
      var amazonEc2Client = ec2Client;

      var describeInstancesResponse = await amazonEc2Client.DescribeInstancesAsync();
      var instances = describeInstancesResponse.Reservations
        .SelectMany(x => x.Instances)
        .Select(x => new
        {
          x.InstanceId,
          x.ImageId,
          Name = GetValue(x.Tags, "Name"),
          x.Tags,
          Volume = x.BlockDeviceMappings.Select(r => r.Ebs.VolumeId).First()
        })
        .Where(x => x.Tags.Any(t => t.Key == TagRetention));
      foreach (var instance in instances)
      {
        var settings = BackupSettings.From(instance.InstanceId, instance.Name, instance.ImageId,
          instance.Tags.GroupBy(x => x.Key).ToDictionary(x => x.Key, x => x.First().Value));
        $"Found AwSnap ({settings.Name} {settings.InstanceId}) for Volume {instance.Volume}".Dump();
        await Backup(amazonEc2Client, instance.Volume, settings);
        await Cleanup(amazonEc2Client, settings);
      }
    }

    private static string GetValue(List<Tag> xTags, string name)
    {
      return xTags.Where(t => t.Key == name).Select(t => t.Value).FirstOrDefault();
    }


    private static async Task Cleanup(AmazonEC2Client client, BackupSettings backupSettings)
    {
      var result = await client.DescribeSnapshotsAsync(new DescribeSnapshotsRequest
      {
        Filters = new List<Filter>
        {
          new Filter {Name = "description", Values = new List<string> {$"*{backupSettings.Name}*"}}
        }
      });
      var validSnapShots =
        result.Snapshots.Where(x => GetValue(x.Tags, "InstanceName") == backupSettings.Name).ToList();
      var keeping = validSnapShots.OrderByDescending(x => x.StartTime).Take(backupSettings.RetentionCount)
        .Select(x => GetValue(x.Tags, "Name")).ToArray();
      var names = string.Join(",", keeping);
      $"Keeping {keeping.Length} snapshots [{names}]".Dump();
      var remove = validSnapShots
        .OrderByDescending(x => x.StartTime)
        .Skip(backupSettings.RetentionCount)
        .ToArray();

      $"Remove {remove.Length} snapshots".Dump();
      foreach (var snapshot in remove)
      {
        $"Removing {snapshot.SnapshotId} - {GetValue(snapshot.Tags, "Name")}.".Dump();
        await client.DeleteSnapshotAsync(new DeleteSnapshotRequest(snapshot.SnapshotId));
      }
    }

    private static async Task Backup(AmazonEC2Client client, string volumeId, BackupSettings settings)
    {
      $"Create AwSnap ({settings.Name} {settings.InstanceId}) for Volume {volumeId}".Dump();
      var result = await client.CreateSnapshotAsync(new CreateSnapshotRequest
      {
        VolumeId = volumeId,
        Description =
          $"Created by AwSnap ({settings.Name} {settings.InstanceId}) for Volume {volumeId}",
        TagSpecifications = new List<TagSpecification>
        {
          new TagSpecification
          {
            Tags = new List<Tag>
            {
              new Tag("Name", $"{settings.Name}-{DateTime.Now:yyyy-MM-dd HH:m}"),
              new Tag("AwSnap", $"true"),
              new Tag("Instance", settings.InstanceId),
              new Tag("InstanceName", settings.Name),
              new Tag("ImageId", settings.ImageId)
            },
            ResourceType = ResourceType.Snapshot
          }
        }
      });
      $"Done with ({settings.Name}-{settings.InstanceId})".Dump();
    }

    #endregion

    #region Nested type: BackupSettings

    public class BackupSettings
    {
      public string InstanceId { get; set; }
      public string ImageId { get; set; }
      public int RetentionCount { get; set; }
      public string Name { get; set; }

      public static BackupSettings From(string instanceId, string name, string imageId,
        Dictionary<string, string> settings)
      {
        settings.TryGetValue(TagRetention, out var awSnapRetention);
        var retentionCount = int.Parse(awSnapRetention);
        return new BackupSettings
        {
          Name = name,
          InstanceId = instanceId,
          RetentionCount = retentionCount,
          ImageId = imageId
        };
      }
    }

    #endregion
    
  }
}