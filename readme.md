# How to run

 docker run --name awsnap  -e "AWS_ACCESS_KEY_ID=$AWS_DEV_ACCESSID" -e "AWS_SECRET_ACCESS_KEY=$AWS_DEV_SECRET" -e "AWS_DEFAULT_REGION=$AWS_DEV_REGION"  -d rolfwessels/awsnap:latest 


# Environment vars

* *AWS_ACCESS_KEY_ID* Sets the amazon keys [Required]
* *AWS_SECRET_ACCESS_KEY* Sets the amazon keys [Required]
* *AWS_DEFAULT_REGION* Sets the amazon keys [Required]
* *AWSNAP_CRON* Sets the time the cron would run [56 02 * * *]


# For developers

*Setup env*

export AWS_DEV_ACCESSID=AAAAAAAAAAAAAAAA
export AWS_DEV_SECRET=AAAAAAAAAAAAAAAA
export AWS_DEV_REGION=us-east-1


*Build*

To build just run `sh build.sh` 


*Debug*

To debug just run `sh debug.sh`.This will open an interactive session where you can build the files in the app folder.

*Run*

To run just execute `sh run.sh` 


*deploy*

 docker login -u="$DOCKER_USERNAME" -p="$DOCKER_PASSWORD" 

 docker push rolfwessels/awsnap 